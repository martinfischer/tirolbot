# Einführung
Tirolbot ist ein [reddit](http://www.reddit.com) Bot, der nach neuen Tirol bezogenen Artikeln auf tt.com und tirol.orf.at sucht und diese dann automatisch ins [Tirol Subreddit](http://www.reddit.com/r/Tirol) postet.

## Achtung

Dieser Bot beachtet Reddits API Limits und ist zugeschnitten auf die Bedürfnisse des Tiroler Subreddits.

## Lizenz - License

The MIT License (MIT)
