from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine("sqlite:///tirolbot.db")
Base = declarative_base()


class Article(Base):
    """
    This is the table where all successfully posted articles are stored
    The table name is: articles
    The columns are: id, time_posted, url, title
    """

    __tablename__ = "articles"

    id = Column(Integer, primary_key=True)
    time_posted = Column(DateTime, nullable=False)
    url = Column(String(300), nullable=False, unique=True)
    title = Column(String(300), nullable=False, unique=False)

    def __init__(self, time_posted, url, title):
        self.time_posted = time_posted
        self.url = url
        self.title = title


Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()
