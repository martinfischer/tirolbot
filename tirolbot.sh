#! /bin/bash
cd /home/martin/workspace/tirolbot
source bin/activate
cd tirolbot

# virtualenv is now active, which means your PATH has been modified and python
# can be called like this.

python tirolbot.py

# a sample crontab entry:
# to check for jobs by the loged in user: crontab -l
# to edit/create the jobs for the loged in user: crontab -e
# entry: @hourly /home/martin/workspace/tirolbot/tirolbot/tirolbot.sh
