SQLAlchemy==0.7.9
beautifulsoup4==4.1.3
distribute==0.6.24
praw==1.0.12
six==1.2.0
wsgiref==0.1.2
