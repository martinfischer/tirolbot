import configparser
import logging
import logging.handlers
import praw
import json
from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
from datetime import datetime
from time import sleep
from models import Article, session


def get_orf(url):
    """
    Retrieves articles from orf.at
    """
    try:
        html = urlopen(url)
        html = html.read().decode()
    except Exception as error:
        html = ""
        log.error("{}".format(error))
    orf = BeautifulSoup(html)
    orf_links = {}
    for a in orf.select(".storyBox a"):
        title = a.get_text()
        if config["orf"]["include"] in a["href"] \
                                and not config["orf"]["exclude1"] in title \
                                and not config["orf"]["exclude2"] in title:
            orf_links[a["href"]] = title

    log.debug("Returning {} articles with get_orf()".format(len(orf_links)))

    return orf_links


def get_tt(url):
    """
    Retrieves articles from tt.com
    """
    try:
        html = urlopen(url)
        html = html.read().decode()
    except Exception as error:
        html = ""
        log.error("{}".format(error))
    tt = BeautifulSoup(html)
    tt_links = {}
    hrefs = tt.select("#kurzmeldungen a")
    titles = tt.select("#kurzmeldungen h3")
    for i in range(len(hrefs)):
        url = hrefs[i]["href"]
        if config["tt"]["exclude"] in url:
            log.debug("Skipping - tt excluded: url: {}".format(url))
            continue
        tt_links["http://tt.com{}".format(url)] = titles[i].get_text()

    log.debug("Returning {} articles with get_tt()".format(len(tt_links)))

    return tt_links


def get_articles():
    """
    Checks if sites are enabled in the config file and returns a dict with all
    retrieved articles
    """
    sites = config["sites"]
    orf = sites.getboolean("orf")
    tt = sites.getboolean("tt")

    orf_articles = {}
    tt_articles = {}

    if orf:
        orf_url = config["orf"]["url"]
        orf_articles = get_orf(orf_url)

    if tt:
        tt_url = config["tt"]["url"]
        tt_articles = get_tt(tt_url)

    articles = orf_articles.copy()
    articles.update(tt_articles)

    log.debug("Returning {} articles with get_articles()".format(
                                                                len(articles)))

    return articles


def post_article(url, title):
    """
    Posts a link to Reddit
    """
    useragent = config["reddit"]["useragent"]
    username = config["reddit"]["username"]
    password = config["reddit"]["password"]
    subreddit = config["reddit"]["subreddit"]

    reddit = praw.Reddit(user_agent=useragent)
    try:
        reddit.login(username, password)
        log.debug("Loging into Reddit as: {}".format(username))
        log.debug("Posting: title: {} - url: {}".format(title, url))

        response = reddit.submit(subreddit, title, url=url)

        return response

    except Exception as error:
        log.error("{}".format(error))


def get_subreddit_posts():
    """
    Gets the urls of the newest 100 posts in the subreddit and returns them
    as a list
    """
    subreddit = config["reddit"]["subreddit"]
    useragent = config["reddit"]["useragent"]
    header = {"User-Agent": " {}".format(useragent)}

    posts = []
    try:
        after = ""
        for i in range(4):
            url = "http://www.reddit.com/r/{}/new.json?sort=new&after={}"\
                                                    .format(subreddit, after)
            req = Request(url=url, data=None, headers=header)
            response = urlopen(req)
            newest_json = response.read().decode()
            newest = json.loads(newest_json)

            for child in newest["data"]["children"]:
                posts.append(child["data"]["url"])

            after = newest["data"]["after"]
            if not after:
                break

            sleep(3)

    except Exception as error:
        log.error("{}".format(error))

    log.debug("Returning {} urls with get_subreddit_posts()".format(
                                                                len(posts)))

    return posts


def url_in_db(url):
    """
    Returns True if the given url is in the database and False if not
    """

    return bool(session.query(Article).filter_by(url=url).count())


def tt_id_allready_posted(url, allready_posted):
    """
    Returns True if the tt id is allready posted
    """
    for posted_url in allready_posted:
        if url.split("/")[-2] in posted_url:

            return True

    return False


def tt_id_in_db(url):
    """
    Returns True if the tt id is in the database and False if not
    """
    db_urls = []
    for db_url in session.query(Article.url).distinct():
        u = db_url[0]
        if u.startswith("http://tt.com"):
            db_urls.append(u)

    for db_url in db_urls:
        if url.split("/")[-2] in db_url:

            return True

    return False


def main():
    """
    Checks if scraped articles are allready posted on reddit or allready in the
    db, if not it posts to reddit and then sleeps for the postdelay
    """
    postdelay = int(config["limits"]["postdelay"])
    log.info("***Starting***")
    allready_posted = get_subreddit_posts()
    articles = get_articles()

    for url, title in articles.items():
        if url in allready_posted:
            log.debug("Skipping - allready posted: "
                      "title: {} - url: {}".format(title, url))
            continue

        if url_in_db(url):
            log.debug("Skipping - allready in db: "
                      "title: {} - url: {}".format(title, url))
            continue

        if url.startswith("http://tt.com") and tt_id_allready_posted(url,
                                                            allready_posted):
            log.debug("Skipping - tt id allready posted: "
                      "title: {} - url: {}".format(title, url))
            continue

        if tt_id_in_db(url):
            log.debug("Skipping - tt id allready in db: "
                      "title: {} - url: {}".format(title, url))
            continue

        post = post_article(url, title)
        if post:
            log.info("Sucessfully posted title: {} - url: {}".format(title,
                                                                    url))
            now = datetime.now()
            article = Article(now, url, title)
            session.add(article)
            try:
                session.commit()
                log.debug("Sucessfully commited to db: title: {} - "
                          "url: {}".format(title, url))
            except Exception as error:
                log.error("{}".format(error))
        else:
            log.error("Not posted to reddit: title: {} - url: {}".format(title,
                                                                    url))

        sleep(postdelay)

    log.info("***Done for now***")

    return "***Done***"


if __name__ == "__main__":
    """
    Setup config
    """
    config = configparser.ConfigParser()
    config.read("config.cfg")

    """
    Setup logging
    """
    log_filename = "tirolbot.log"
    log = logging.getLogger("tirolbot")
    log.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    file_handler = logging.handlers.RotatingFileHandler(log_filename,
                                                    maxBytes=5242880,  # 5 MB
                                                    backupCount=10,
                                                    )
    file_handler.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    # create formatter and add it to the handlers
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - "
                                  "%(message)s")
    console_handler.setFormatter(formatter)
    file_handler.setFormatter(formatter)
    # add the handlers to log
    log.addHandler(console_handler)
    log.addHandler(file_handler)

    """
    Starts the show
    """
    main()
